use std::ops::Deref;
use std::ptr::NonNull;

/// # Safety
/// Only trivially constructible ZSTs are allowed to conform to this trait.
pub(crate) unsafe trait StaticDeref: 'static + Sized + Deref {
    fn static_deref() -> &'static Self::Target {
        Self::deref(unsafe { NonNull::dangling().as_ref() })
    }
}
