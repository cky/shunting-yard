use thiserror::Error;

use super::common::{BinaryOperator, Node, OperatorQuery, Parser, TernaryOperator, ToNode, Token, UnaryOperator};

pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Clone, Debug, Eq, Error, PartialEq)]
pub enum ParseError {
    #[error("unexpected end of input")]
    UnexpectedEndOfInput,
    #[error("invalid token: {0}")]
    InvalidToken(String),
    #[error("stack underflow")]
    StackUnderflow,
}

#[derive(Clone, Debug, Default, PartialEq)]
struct ParseState {
    stack: Vec<Node>,
}

impl Parser for ParseState {
    type Error = ParseError;

    fn feed(&mut self, token: Token) -> ParseResult<()> {
        let result = match token {
            Token::Literal(value) => Node::Literal(value),
            Token::Symbol(symbol) if BinaryOperator::contains(symbol) => {
                if let (Some(rhs), Some(lhs)) = (self.stack.pop(), self.stack.pop()) {
                    BinaryOperator::get(symbol).unwrap().to_node((lhs, rhs))
                } else {
                    return Err(ParseError::StackUnderflow);
                }
            }
            Token::Symbol(symbol) if UnaryOperator::contains(symbol) => {
                if let Some(arg) = self.stack.pop() {
                    UnaryOperator::get(symbol).unwrap().to_node((arg,))
                } else {
                    return Err(ParseError::StackUnderflow);
                }
            }
            Token::Symbol(symbol) if TernaryOperator::contains(symbol) => {
                if let (Some(rhs), Some(mhs), Some(lhs)) = (self.stack.pop(), self.stack.pop(), self.stack.pop()) {
                    TernaryOperator::get(symbol).unwrap().to_node((lhs, mhs, rhs))
                } else {
                    return Err(ParseError::StackUnderflow);
                }
            }
            token => return Err(ParseError::InvalidToken(token.to_string())),
        };
        self.stack.push(result);
        Ok(())
    }

    fn result(mut self) -> ParseResult<Node> {
        if self.stack.len() == 1 {
            Ok(self.stack.pop().unwrap())
        } else {
            Err(ParseError::UnexpectedEndOfInput)
        }
    }
}

pub fn parse<I: IntoIterator<Item = Token>>(tokens: I) -> ParseResult<Node> {
    ParseState::parse(tokens)
}
