use std::cmp::Ordering;
use std::fmt;
use std::str::FromStr;

use fnv::FnvHashMap;
use lazy_static::lazy_static;
use thiserror::Error;

use super::hacks::StaticDeref;

pub trait Parser: Default {
    type Error;

    fn feed(&mut self, token: Token) -> Result<(), Self::Error>;
    fn result(self) -> Result<Node, Self::Error>;

    fn parse<I: IntoIterator<Item = Token>>(tokens: I) -> Result<Node, Self::Error> {
        let mut state = Self::default();
        tokens.into_iter().try_for_each(|token| state.feed(token))?;
        state.result()
    }
}

pub type TokenResult<T> = Result<T, TokenError>;

#[derive(Clone, Debug, Error, Eq, PartialEq)]
pub enum TokenError {
    #[error("invalid token: {0}")]
    InvalidToken(String),
    #[error("operator not found: {0}")]
    OperatorNotFound(String),
}

pub type EvaluationResult<T> = Result<T, EvaluationError>;

#[derive(Clone, Debug, Error, PartialEq)]
#[error("evaluation error: {0}")]
pub struct EvaluationError(Node);

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Token {
    Literal(f64),
    Symbol(&'static str),
    LeftParen,
    RightParen,
}

impl ToString for Token {
    fn to_string(&self) -> String {
        match *self {
            Self::Literal(value) => value.to_string(),
            Self::Symbol(value) => value.to_string(),
            Self::LeftParen => "(".to_string(),
            Self::RightParen => ")".to_string(),
        }
    }
}

impl FromStr for Token {
    type Err = TokenError;

    fn from_str(s: &str) -> TokenResult<Self> {
        if s == "(" {
            Ok(Self::LeftParen)
        } else if s == ")" {
            Ok(Self::RightParen)
        } else if let Some(sym) = BinaryOperator::intern(s)
            .or_else(|| UnaryOperator::intern(s))
            .or_else(|| TernaryOperator::intern(s))
        {
            Ok(Self::Symbol(sym))
        } else if let Ok(value) = s.parse() {
            Ok(Self::Literal(value))
        } else {
            Err(TokenError::InvalidToken(s.to_owned()))
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(crate) enum Associativity {
    Left,
    Right,
}

pub(crate) trait ToNode {
    type Args;

    fn to_node(&self, args: Self::Args) -> Node;
}

pub(crate) trait OperatorQuery: 'static {
    fn map() -> &'static FnvHashMap<&'static str, &'static Self>;

    fn contains(name: &str) -> bool {
        Self::map().contains_key(name)
    }

    fn intern(name: &str) -> Option<&'static str> {
        Self::map().get_key_value(name).map(|(&key, _)| key)
    }

    fn get(name: &str) -> Option<&'static Self> {
        Self::map().get(name).copied()
    }
}

trait Operator: 'static {
    type Map: StaticDeref<Target = FnvHashMap<&'static str, &'static Self>>;
}

impl<T: Operator> OperatorQuery for T {
    fn map() -> &'static FnvHashMap<&'static str, &'static Self> {
        T::Map::static_deref()
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(crate) struct UnaryOperator {
    name: &'static str,
    func: fn(f64) -> f64,
}

impl Operator for UnaryOperator {
    type Map = UNARY_OPS;
}

impl ToNode for UnaryOperator {
    type Args = (Node,);

    fn to_node(&self, args: Self::Args) -> Node {
        Node::Unary(self.name, self.func, Box::new(args))
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(crate) struct BinaryOperator {
    name: &'static str,
    func: fn(f64, f64) -> f64,
    precedence: usize,
    associativity: Associativity,
}

impl Operator for BinaryOperator {
    type Map = BINARY_OPS;
}

impl ToNode for BinaryOperator {
    type Args = (Node, Node);

    fn to_node(&self, args: Self::Args) -> Node {
        Node::Binary(self.name, self.func, Box::new(args))
    }
}

impl PartialOrd for BinaryOperator {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (
            self.precedence.cmp(&other.precedence),
            self.associativity,
            other.associativity,
        ) {
            (Ordering::Equal, Associativity::Left, Associativity::Left) => Some(Ordering::Greater),
            (Ordering::Equal, Associativity::Right, Associativity::Right) => Some(Ordering::Less),
            (Ordering::Equal, ..) => None,
            (ordering, ..) => Some(ordering),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub(crate) struct TernaryOperator {
    name: &'static str,
    func: fn(f64, f64, f64) -> f64,
}

impl Operator for TernaryOperator {
    type Map = TERNARY_OPS;
}

impl ToNode for TernaryOperator {
    type Args = (Node, Node, Node);

    fn to_node(&self, args: Self::Args) -> Node {
        Node::Ternary(self.name, self.func, Box::new(args))
    }
}

#[rustfmt::skip]
lazy_static! {
    static ref UNARY_OPS: FnvHashMap<&'static str, &'static UnaryOperator> = [
        &UnaryOperator { name: "-", func: std::ops::Neg::neg },
        &UnaryOperator { name: "sqrt", func: f64::sqrt },
        &UnaryOperator { name: "cbrt", func: f64::cbrt },
    ]
    .into_iter()
    .map(|op| (op.name, op))
    .collect();

    static ref BINARY_OPS: FnvHashMap<&'static str, &'static BinaryOperator> = [
        &BinaryOperator { name: "+", func: std::ops::Add::add, precedence: 0, associativity: Associativity::Left },
        &BinaryOperator { name: "-", func: std::ops::Sub::sub, precedence: 0, associativity: Associativity::Left },
        &BinaryOperator { name: "*", func: std::ops::Mul::mul, precedence: 1, associativity: Associativity::Left },
        &BinaryOperator { name: "/", func: std::ops::Div::div, precedence: 1, associativity: Associativity::Left },
        &BinaryOperator { name: "%", func: std::ops::Rem::rem, precedence: 1, associativity: Associativity::Left },
        &BinaryOperator { name: "**", func: f64::powf, precedence: 2, associativity: Associativity::Right },
    ]
    .into_iter()
    .map(|op| (op.name, op))
    .collect();

    static ref TERNARY_OPS: FnvHashMap<&'static str, &'static TernaryOperator> = [
        &TernaryOperator { name: "fma", func: f64::mul_add },
    ]
    .into_iter()
    .map(|op| (op.name, op))
    .collect();
}

unsafe impl StaticDeref for UNARY_OPS {}
unsafe impl StaticDeref for BINARY_OPS {}
unsafe impl StaticDeref for TERNARY_OPS {}

#[derive(Clone, Debug, PartialEq)]
pub enum Node {
    Literal(f64),
    Unary(&'static str, fn(f64) -> f64, Box<(Node,)>),
    Binary(&'static str, fn(f64, f64) -> f64, Box<(Node, Node)>),
    Ternary(&'static str, fn(f64, f64, f64) -> f64, Box<(Node, Node, Node)>),
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            Self::Literal(value) => write!(f, "{value}"),
            Self::Unary(name, _, box (ref arg,)) => write!(f, "({name} {arg})"),
            Self::Binary(name, _, box (ref arg1, ref arg2)) => write!(f, "({name} {arg1} {arg2})"),
            Self::Ternary(name, _, box (ref arg1, ref arg2, ref arg3)) => {
                write!(f, "({name} {arg1} {arg2} {arg3})")
            }
        }
    }
}

impl Node {
    pub fn evaluate(&self) -> EvaluationResult<f64> {
        let result = match *self {
            Self::Literal(value) => value,
            Self::Unary(_, func, box (ref arg,)) => func(arg.evaluate()?),
            Self::Binary(_, func, box (ref arg1, ref arg2)) => func(arg1.evaluate()?, arg2.evaluate()?),
            Self::Ternary(_, func, box (ref arg1, ref arg2, ref arg3)) => {
                func(arg1.evaluate()?, arg2.evaluate()?, arg3.evaluate()?)
            }
        };
        if result.is_nan() {
            Err(EvaluationError(self.clone()))
        } else {
            Ok(result)
        }
    }
}
