use thiserror::Error;

use super::common::{BinaryOperator, Node, OperatorQuery, Parser, ToNode, Token, TokenError, UnaryOperator};

pub type ParseResult<T> = Result<T, ParseError>;

#[derive(Clone, Copy, Debug, Eq, Error, PartialEq)]
pub enum ParseError {
    #[error("unexpected end of input")]
    UnexpectedEndOfInput,
    #[error("unexpected operator")]
    UnexpectedOperator,
    #[error("unexpected value")]
    UnexpectedValue,
    #[error("unmatched parenthesis")]
    UnmatchedParenthesis,
    #[error("invalid state")]
    InvalidState,
}

impl From<TokenError> for ParseError {
    fn from(_: TokenError) -> Self {
        Self::UnexpectedOperator
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
enum TokenState {
    #[default]
    Initial,
    HasNumber,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Operator {
    Paren,
    Unary(UnaryOperator),
    Binary(BinaryOperator),
}

#[derive(Clone, Debug, Default, PartialEq)]
struct ParseState {
    state: TokenState,
    operator_stack: Vec<Operator>,
    operand_stack: Vec<Node>,
}

impl Parser for ParseState {
    type Error = ParseError;

    fn feed(&mut self, token: Token) -> ParseResult<()> {
        match self.state {
            TokenState::Initial => self.feed_initial(token),
            TokenState::HasNumber => self.feed_has_number(token),
        }
    }

    fn result(mut self) -> ParseResult<Node> {
        self.pop_tail()?;
        if self.operator_stack.is_empty() && self.operand_stack.len() == 1 {
            Ok(self.operand_stack.pop().unwrap())
        } else {
            Err(ParseError::InvalidState)
        }
    }
}

impl ParseState {
    fn feed_initial(&mut self, token: Token) -> ParseResult<()> {
        match token {
            Token::Literal(value) => self.push_literal(value),
            Token::LeftParen => self.push_paren(),
            Token::Symbol(symbol) if UnaryOperator::contains(symbol) => self.push_unary(symbol),
            _ => Err(ParseError::UnexpectedValue),
        }
    }

    fn feed_has_number(&mut self, token: Token) -> ParseResult<()> {
        match token {
            Token::RightParen => self.pop_paren(),
            Token::Symbol(symbol) if BinaryOperator::contains(symbol) => self.push_binary(symbol),
            _ => Err(ParseError::UnexpectedOperator),
        }
    }

    fn push_paren(&mut self) -> ParseResult<()> {
        self.operator_stack.push(Operator::Paren);
        Ok(())
    }

    fn push_literal(&mut self, value: f64) -> ParseResult<()> {
        self.operand_stack.push(Node::Literal(value));
        self.state = TokenState::HasNumber;
        Ok(())
    }

    fn push_unary(&mut self, name: &str) -> ParseResult<()> {
        self.operator_stack
            .push(Operator::Unary(*UnaryOperator::get(name).unwrap()));
        Ok(())
    }

    fn push_binary(&mut self, name: &str) -> ParseResult<()> {
        let op = BinaryOperator::get(name).unwrap();
        self.pop_higher_precedence(op)?;
        self.operator_stack.push(Operator::Binary(*op));
        self.state = TokenState::Initial;
        Ok(())
    }

    fn pop_higher_precedence(&mut self, op: &BinaryOperator) -> ParseResult<()> {
        while let Some(last_op) = self.operator_stack.pop() {
            match last_op {
                Operator::Unary(op1) => self.process_unary(op1)?,
                Operator::Binary(op2) if op2 > *op => self.process_binary(op2)?,
                last_op => {
                    self.operator_stack.push(last_op);
                    break;
                }
            }
        }
        Ok(())
    }

    fn pop_paren(&mut self) -> ParseResult<()> {
        while let Some(op) = self.operator_stack.pop() {
            match op {
                Operator::Paren => return Ok(()),
                Operator::Unary(op1) => self.process_unary(op1)?,
                Operator::Binary(op2) => self.process_binary(op2)?,
            }
        }
        Err(ParseError::UnmatchedParenthesis)
    }

    fn pop_tail(&mut self) -> ParseResult<()> {
        if self.state != TokenState::HasNumber {
            return Err(ParseError::UnexpectedEndOfInput);
        }

        while let Some(op) = self.operator_stack.pop() {
            match op {
                Operator::Paren => return Err(ParseError::UnmatchedParenthesis),
                Operator::Unary(op1) => self.process_unary(op1)?,
                Operator::Binary(op2) => self.process_binary(op2)?,
            }
        }
        Ok(())
    }

    fn process_unary(&mut self, op: UnaryOperator) -> ParseResult<()> {
        if let Some(value) = self.operand_stack.pop() {
            self.operand_stack.push(op.to_node((value,)));
            Ok(())
        } else {
            Err(ParseError::InvalidState)
        }
    }

    fn process_binary(&mut self, op: BinaryOperator) -> ParseResult<()> {
        if let (Some(rhs), Some(lhs)) = (self.operand_stack.pop(), self.operand_stack.pop()) {
            self.operand_stack.push(op.to_node((lhs, rhs)));
            Ok(())
        } else {
            Err(ParseError::InvalidState)
        }
    }
}

pub fn parse<I: IntoIterator<Item = Token>>(tokens: I) -> ParseResult<Node> {
    ParseState::parse(tokens)
}
