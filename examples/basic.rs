use std::error::Error;

use anyhow::Result;

use shunting_yard::common::{Node, Token};
use shunting_yard::infix::parse as parse_infix;
use shunting_yard::postfix::parse as parse_postfix;

fn main() -> Result<()> {
    print_eval_infix("sqrt ( 3 ** 2 + 4 ** 2 )");
    print_eval_infix("cbrt - ( 3 ** 3 + 4 ** 3 )");
    print_eval_infix("1 / 0");
    print_eval_infix("1 / 0 - 1 / 0");
    print_eval_postfix("3 2 ** 4 2 ** + sqrt");
    print_eval_postfix("3 3 ** 4 3 ** + - cbrt");
    print_eval_postfix("3 2 ** 4 2 **");
    print_eval_postfix("3 4 5 fma");
    Ok(())
}

fn print_eval_infix(expression: &str) {
    match evaluate(parse_infix, expression) {
        Ok(result) => println!("infix[{expression}] => {result}"),
        Err(error) => eprintln!("infix[{expression}] => {error}"),
    }
}

fn print_eval_postfix(expression: &str) {
    match evaluate(parse_postfix, expression) {
        Ok(result) => println!("postfix[{expression}] => {result}"),
        Err(error) => eprintln!("postfix[{expression}] => {error}"),
    }
}

fn evaluate<E: 'static + Error + Send + Sync, F: FnOnce(Vec<Token>) -> Result<Node, E>>(
    parse: F,
    expression: &str,
) -> Result<f64> {
    let tokens = expression
        .split_ascii_whitespace()
        .map(str::parse)
        .collect::<Result<Vec<Token>, _>>()?;
    Ok(parse(tokens)?.evaluate()?)
}
